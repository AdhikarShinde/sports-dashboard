import React from "react";
import { Panel, Table, Button, IconButton } from "rsuite";
import PlusIcon from "@rsuite/icons/Plus";
import Styles from "./Panels.module.css";

const { Column, HeaderCell, Cell } = Table;

interface PanelsProps {
  team: any;
  index: number;
  handleOpen: (value: any, isEdit: boolean) => void;
}

const Panels: React.FC<PanelsProps> = ({ team, index, handleOpen }) => {
  return (
    <Panel
      key={`${team.team_name}_${index}`}
      header={`${team.team_name}`}
      eventKey={index}
      id={`${team.team_name}-${index}`}
    >
      <div className={Styles.PanelHead}>
        <span className={Styles.Title}>
          Total Players in {team.team_name} are{" "}
          <span className={Styles.Count}>{team.players.length}</span>
        </span>
        <IconButton
          color="blue"
          appearance="primary"
          onClick={() => handleOpen(team.team_name, false)}
          icon={<PlusIcon />}
        >
          Add Player
        </IconButton>
      </div>
      <Table
        height={400}
        width={480}
        data={team.players}
        hover={true}
        autoHeight={true}
        onRowClick={(rowData, rowIndex) => {
          console.log("row data", rowData);
        }}
      >
        <Column width={200}>
          <HeaderCell>First Name</HeaderCell>
          <Cell dataKey="name" />
        </Column>
        <Column width={200}>
          <HeaderCell>Age</HeaderCell>
          <Cell dataKey="age" />
        </Column>

        <Column width={80} fixed="right">
          <HeaderCell>Action</HeaderCell>
          <Cell style={{ padding: "6px" }}>
            {(rowData) => (
              <Button
                appearance="link"
                onClick={() => handleOpen(rowData, true)}
              >
                Edit
              </Button>
            )}
          </Cell>
        </Column>
      </Table>
    </Panel>
  );
};

export default Panels;
