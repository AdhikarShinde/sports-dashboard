import React from "react";
import Styles from "./GameCard.module.css";
import { useNavigate } from "react-router-dom";

interface Player {
  name: string;
  age: number;
}

interface Team {
  team_name: string;
  players: Player[];
}

interface Game {
  game: string;
  teams: Team[];
}

interface GameCardProps {
  gameData: Game;
}

const GameCard: React.FC<GameCardProps> = ({ gameData }) => {
  const { game, teams } = gameData;
  const navigate = useNavigate();
  const handleToggleTeams = () => {
    navigate(`/sports/${game}`);
  };

  return (
    <div className={Styles.card} onClick={handleToggleTeams}>
      <h3>{game}</h3>
      <p>Total Teams: {teams.length}</p>
      <p className={Styles.viewTeams}>Click to view teams</p>
    </div>
  );
};

export default GameCard;
