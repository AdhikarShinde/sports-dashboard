import React, { FC, useState } from "react";
import { Sidenav, Nav } from "rsuite";
import MagicIcon from "@rsuite/icons/legacy/Magic";
import { useNavigate } from "react-router-dom";
import Styles from "./Navbar.module.css";

interface Player {
  name: string;
  age: number;
}

interface Team {
  team_name: string;
  players: Player[];
}

interface Game {
  game: string;
  teams: Team[];
}

interface NavbarProps {
  games: Game[];
}

const Navbar: FC<NavbarProps> = ({ games }) => {
  const navigate = useNavigate();
  const [activeKey, setActiveKey] = useState("1");
  const onClickSports = (e: React.MouseEvent) => {
    e.preventDefault();
    navigate(`/`);
  };

  const navClick = (e: React.MouseEvent, navigateTo: string) => {
    e.preventDefault();
    e.stopPropagation();
    console.log(navigateTo);
    navigate(`/sports/${navigateTo}`);
  };
  if (!games.length) return <></>;
  return (
    <div style={{ width: 240, height: "100%" }}>
      <Sidenav expanded={true} defaultOpenKeys={["1"]}>
        <Sidenav.Header>
          <div className={Styles.headerStyles}>Sports Dashboard</div>
        </Sidenav.Header>
        <Sidenav.Body>
          <Nav activeKey={activeKey} onSelect={setActiveKey}>
            <Nav.Menu
              placement="rightStart"
              eventKey="1"
              title="Sports"
              onClick={onClickSports}
              icon={<MagicIcon />}
            >
              {games.length &&
                games.map((game, index) => (
                  <Nav.Item
                    onClick={(e: React.MouseEvent) => navClick(e, game.game)}
                    key={game.game}
                    eventKey={`3-${index}`}
                  >
                    {game.game}
                  </Nav.Item>
                ))}
            </Nav.Menu>
          </Nav>
        </Sidenav.Body>
      </Sidenav>
    </div>
  );
};

export default Navbar;
