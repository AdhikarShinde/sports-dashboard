import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Loader } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Navbar from "./components/Navbar/Navbar";
import AppRoutes from "./routes/routes";
import Styles from "./App.module.css";
import { useSelector } from "react-redux";
import { fetchGames } from "./features/sports";
import { useAppDispatch } from "./app/store";

interface Player {
  name: string;
  age: number;
}

interface Team {
  team_name: string;
  players: Player[];
}

interface Game {
  game: string;
  teams: Team[];
}

interface GameState {
  games: Game[];
  loading: boolean;
  error: string | null;
}

function App() {
  const dispatch = useAppDispatch();
  const games = useSelector((state: { games: GameState }) => state.games.games);
  const loading = useSelector(
    (state: { games: GameState }) => state.games.loading
  );
  useEffect(() => {
    dispatch(fetchGames());
  }, [dispatch]);
  return (
    <Router>
      <div className={Styles.App}>
        <Navbar games={games} />
        {loading ? (
          <Loader center={true} size="lg" content="Loading Please wait..." />
        ) : (
          <div className={Styles.ContentBlock}>
            <AppRoutes />
          </div>
        )}
      </div>
    </Router>
  );
}

export default App;
