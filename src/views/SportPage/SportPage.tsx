import React, { useCallback, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { PanelGroup, Button, Modal, Input } from "rsuite";
import Styles from "./SportPage.module.css";
import { GameState, addPlayer, editPlayer } from "../../features/sports";
import { useAppDispatch } from "../../app/store";
import Panels from "../../components/Panels/Panels";

interface SportPageProps {}

const SportPage: React.FC<SportPageProps> = () => {
  let { sport } = useParams();
  const [open, setOpen] = useState(false);
  const [originalPlayerName, setOriginalPlayerName] = useState<string | null>(
    null
  );
  const dispatch = useAppDispatch();
  const handleClose = () => setOpen(false);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const games = useSelector((state: { games: GameState }) => state.games.games);
  const [selectedPlayer, setSelectedPlayer] = useState<any>(null);
  const currentGame = games?.find((game) => game.game === sport);

  const handleOpen = useCallback((value: any, isEdit: boolean = true) => {
    setSelectedPlayer(value);
    setIsEditMode(isEdit);

    if (!isEdit) {
      setOriginalPlayerName(value);
      setSelectedPlayer(null);
    } else {
      setOriginalPlayerName(value.name);
    }
    setOpen(true);
  }, []);

  const handleEdit = useCallback(() => {
    if (isEditMode) {
      console.log(isEditMode);
      console.log(selectedPlayer, currentGame, originalPlayerName);
      if (selectedPlayer && currentGame && originalPlayerName) {
        console.log(selectedPlayer, currentGame, originalPlayerName);
        const gameIndex = games.findIndex(
          (game) => game.game === currentGame.game
        );
        if (gameIndex !== -1) {
          let teamIndex = -1;
          let playerIndex = -1;
          games[gameIndex].teams.forEach((team, tIndex) => {
            const foundPlayerIndex = team.players.findIndex(
              (player) => player.name === originalPlayerName
            );

            if (foundPlayerIndex !== -1) {
              teamIndex = tIndex;
              playerIndex = foundPlayerIndex;
            }
          });

          if (teamIndex !== -1 && playerIndex !== -1) {
            dispatch(
              editPlayer({
                gameIndex,
                teamIndex,
                playerIndex,
                player: selectedPlayer,
              })
            );
            handleClose();
          }
        }
      }
    } else {
      console.log(isEditMode);
      if (currentGame && selectedPlayer) {
        const gameIndex = games.findIndex(
          (game) => game.game === currentGame.game
        );

        if (gameIndex !== -1) {
          let teamIndex = -1;
          games[gameIndex].teams.forEach((team, tIndex) => {
            console.log(originalPlayerName);
            if (team.team_name === originalPlayerName) teamIndex = tIndex;
          });
          console.log(teamIndex);
          if (teamIndex !== -1) {
            console.log(teamIndex);
            dispatch(
              addPlayer({
                gameIndex,
                teamIndex,
                player: selectedPlayer,
              })
            );
            handleClose();
          }
        }
      }
    }
  }, [
    isEditMode,
    selectedPlayer,
    currentGame,
    originalPlayerName,
    games,
    dispatch,
  ]);

  return (
    <>
      <span className={Styles.Title}>
        Here the List of Teams for {currentGame?.game}
      </span>
      <div className={Styles.ListContainer}>
        <PanelGroup accordion bordered>
          {currentGame?.teams.map((team, index) => (
            <Panels team={team} index={index} handleOpen={handleOpen} />
          ))}
        </PanelGroup>
        <Modal size={"sm"} backdrop={true} open={open} onClose={handleClose}>
          <Modal.Header>
            <Modal.Title>Add/Edit Player Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <span>Name</span>
            <Input
              defaultValue={selectedPlayer?.name}
              type="text"
              onChange={(value) =>
                setSelectedPlayer({ ...selectedPlayer, name: value })
              }
            />
            <span>Age</span>
            <Input
              type="number"
              defaultValue={selectedPlayer?.age}
              onChange={(value) =>
                setSelectedPlayer({ ...selectedPlayer, age: Number(value) })
              }
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleClose} appearance="subtle">
              Cancel
            </Button>
            <Button onClick={handleEdit} appearance="primary">
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </>
  );
};

export default SportPage;
