import React from "react";
import Styles from "./SportsPage.module.css";
import { GameState } from "../../features/sports";
import { useSelector } from "react-redux";
import GameCard from "../../components/GameCard/GameCard";

const SportsPage = () => {
  const games = useSelector((state: { games: GameState }) => state.games.games);
  if (!games.length) return <></>;
  return (
    <>
      <div className={Styles.Container}>
        <span className={Styles.Title}>Here is the List of all the Sports</span>
        <div className={Styles.GameList}>
          {games &&
            games.length &&
            games.map((game: any, index) => (
              <GameCard key={index} gameData={game} />
            ))}
        </div>
      </div>
    </>
  );
};

export default SportsPage;
