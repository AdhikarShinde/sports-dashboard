import { configureStore } from "@reduxjs/toolkit";
import gamesReducer from "../features/sports";
import { useDispatch } from "react-redux";

export const store = configureStore({
  reducer: {
    games: gamesReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
