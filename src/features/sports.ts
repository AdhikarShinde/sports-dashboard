import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";

interface Player {
  name?: string;
  age: number;
}

interface Team {
  team_name: string;
  players: Player[];
}

interface Game {
  game: string;
  teams: Team[];
}

export interface GameState {
  games: Game[];
  loading: boolean;
  error: string | null;
}

const initialState: GameState = {
  games: [],
  loading: false,
  error: null,
};

export const fetchGames = createAsyncThunk("games/fetchGames", async () => {
  const response = await axios.get<Game[]>(
    "https://mocki.io/v1/b4544a37-0765-405f-baf6-6675845d5a0e"
  );
  return response.data;
});

const gamesSlice = createSlice({
  name: "games",
  initialState,
  reducers: {
    addPlayer: (
      state,
      action: PayloadAction<{
        gameIndex: number;
        teamIndex: number;
        player: Player;
      }>
    ) => {
      const { gameIndex, teamIndex, player } = action.payload;
      state.games[gameIndex].teams[teamIndex].players.unshift(player);
    },
    editPlayer: (
      state,
      action: PayloadAction<{
        gameIndex: number;
        teamIndex: number;
        playerIndex: number;
        player: Player;
      }>
    ) => {
      const { gameIndex, teamIndex, playerIndex, player } = action.payload;
      state.games[gameIndex].teams[teamIndex].players[playerIndex] = player;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGames.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchGames.fulfilled, (state, action) => {
        state.loading = false;
        state.games = action.payload;
      })
      .addCase(fetchGames.rejected, (state, action) => {
        state.loading = false;
        // state.error = action.error.message;
      });
  },
});

export const { addPlayer, editPlayer } = gamesSlice.actions;

export default gamesSlice.reducer;
