interface Player {
  name: string;
  age: number;
}

interface Team {
  team_name: string;
  players: Player[];
}

interface Game {
  game: string;
  teams: Team[];
}

const fetchData = async (): Promise<Game[]> => {
  try {
    const response = await fetch(
      "https://mocki.io/v1/b4544a37-0765-405f-baf6-6675845d5a0e"
    );
    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }
    const data: Game[] = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data:", error);
    throw error;
  }
};

export default fetchData;
