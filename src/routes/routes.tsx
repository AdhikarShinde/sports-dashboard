import React from "react";
import { Routes, Route } from "react-router-dom";
import SportsPage from "../views/SportsPage/SportsPage";
import SportPage from "../views/SportPage/SportPage";

interface RouteType {
  path: string;
  element: React.ReactElement;
  key: string;
  children?: RouteType[];
}

const routes: RouteType[] = [
  {
    path: "/",
    element: <SportsPage />,
    key: "sportsPage",
  },
  {
    path: "/sports/:sport",
    element: <SportPage />,
    key: "sportPage",
  },
];

const AppRoutes: React.FC = () => {
  return (
    <Routes>
      {routes.map((route) => (
        <Route {...route}>
          {route.children &&
            route.children.map((childRoute) => (
              <Route
                path={childRoute.path}
                element={childRoute.element}
                key={childRoute.key}
              />
            ))}
        </Route>
      ))}
    </Routes>
  );
};

export default AppRoutes;
