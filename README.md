# create-react-app-with-typescript

## How to use

Install it and run:

```
npm install
npm run start
# or
yarn
yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
